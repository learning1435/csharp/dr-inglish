namespace dr_inglish
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DrInglish : DbContext
    {
        public DrInglish()
            : base("name=DrInglish")
        {
        }

        public virtual DbSet<Word> Words { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Word>()
                .Property(e => e.Id)
                .IsRequired();

            modelBuilder.Entity<Word>()
                .Property(e => e.Polish)
                .IsUnicode(false);

            modelBuilder.Entity<Word>()
                .Property(e => e.English)
                .IsUnicode(false);

            modelBuilder.Entity<Word>()
                .Property(e => e.Type)
                .IsUnicode(false);
        }
    }
}
