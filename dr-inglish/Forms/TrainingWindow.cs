﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dr_inglish
{
    public partial class TrainingWindow : Form
    {
        private Word[] words { get; set; }

        private int currentIndex { get; set; }

        private int wordsCount { get; set; }

        private int allGoodsAnswers { get; set; }

        public TrainingWindow()
        {
            InitializeComponent();
        }

        public void OnWindowLoad(object sender, EventArgs args)
        {
            using (var dbContext = new DrInglish())
            {
                this.words = dbContext.Words.Take(20).ToArray();
                this.wordsCount = this.words.Length;
                this.currentIndex = 0;
            }

            SetUpWindow();
        }

        public void NextWord(object sender, EventArgs args)
        {
            if (currentIndex == (wordsCount - 1))
            {
                ShowResults();
                return;
            }

            var correctAnswer = this.words[this.currentIndex].English.ToLower();
            if (correctAnswer == this.currentWordInput.Text.ToLower())
            {
                this.allGoodsAnswers++;
            }

            if (currentIndex + 1 == (wordsCount - 1))
            {
                this.nextWordsBtn.Text = "Zakończ";
            }

            currentIndex++;
            UpdateUI();
        }

        private void ShowResults()
        {
            this.Controls.Clear();

            var exitWindowBtn = new Button();
            exitWindowBtn.BackColor = System.Drawing.Color.Transparent;
            exitWindowBtn.BackgroundImage = global::dr_inglish.Properties.Resources.Button_noshadow;
            exitWindowBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            exitWindowBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            exitWindowBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            exitWindowBtn.Font = new System.Drawing.Font("Raleway", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            exitWindowBtn.ForeColor = System.Drawing.Color.White;
            exitWindowBtn.Location = new System.Drawing.Point(313, 411);
            exitWindowBtn.Margin = new System.Windows.Forms.Padding(0);
            exitWindowBtn.Name = "exitWindowBtn";
            exitWindowBtn.Size = new System.Drawing.Size(193, 47);
            exitWindowBtn.TabIndex = 5;
            exitWindowBtn.Text = "Wyjdź";
            exitWindowBtn.Click += new System.EventHandler(this.ExitWindow);
            exitWindowBtn.UseVisualStyleBackColor = false;

            var resultsTextLabel = new Label();
            resultsTextLabel.AutoSize = true;
            resultsTextLabel.Location = new System.Drawing.Point(55, 43);
            resultsTextLabel.MinimumSize = new System.Drawing.Size(200, 0);
            resultsTextLabel.Name = "resultsTextLabel";
            resultsTextLabel.Size = new System.Drawing.Size(200, 19);
            resultsTextLabel.TabIndex = 0;
            resultsTextLabel.Text = $"Wynik to {this.allGoodsAnswers} z {this.wordsCount}";
            resultsTextLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;

            var resultsContainer = new GroupBox(); 
            resultsContainer.BackColor = System.Drawing.Color.Transparent;
            resultsContainer.Controls.Add(resultsTextLabel);
            resultsContainer.Font = new System.Drawing.Font("Raleway", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            resultsContainer.ForeColor = System.Drawing.Color.White;
            resultsContainer.Location = new System.Drawing.Point(251, 209);
            resultsContainer.Name = "resultsContainer";
            resultsContainer.Size = new System.Drawing.Size(308, 89);
            resultsContainer.TabIndex = 7;
            resultsContainer.TabStop = false;
            resultsContainer.Text = "Wynik";

            this.Controls.Add(resultsContainer);
            this.Controls.Add(exitWindowBtn);
        }

        private void ExitWindow(object sender, EventArgs args)
        {
            this.Close();
        }

        private void SetUpWindow()
        {
            this.allWordsCount.Text = wordsCount.ToString();
            this.allGoodsAnswers = 0;
            UpdateUI();
        }

        private void UpdateUI()
        {
            this.currentWordIndex.Text = (currentIndex + 1).ToString();
            this.wordToTranslate.Text = this.words[this.currentIndex].Polish;
            this.currentWordInput.Text = "";
        }
    }
}
