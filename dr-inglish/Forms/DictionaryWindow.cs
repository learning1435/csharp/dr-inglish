﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using dr_inglish.Forms;
using dr_inglish.Helpers;

namespace dr_inglish
{
    public partial class DictionaryWindow : Form
    {

        private TableLayoutPanel WordsContainer { get; set; }

        private List<Word> Words { get; set; }

        public DictionaryWindow()
        {
            InitializeComponent();
            this.Load += this.OnLoadWindow;
        }

        private void OnLoadWindow(Object sender, EventArgs eventArgs)
        {
            this.level.DataSource = new LevelOption[]
            {
                new LevelOption {Level = Levels.Beginner, Label = "Początkujący"},
                new LevelOption {Level = Levels.Medium, Label = "Zaawansowany" },
                new LevelOption {Level = Levels.Expert, Label = "Ekspert"}
            };

            this.level.DisplayMember = "Label";
            this.level.ValueMember = "Level";
            this.level.SelectedItem = this.level.Items[0];

            this.wordType.DataSource = new WordTypeOption[]
            {
                new WordTypeOption {Type = Types.Noun, Label = "Rzeczownik"},
                new WordTypeOption {Type = Types.Verb, Label = "Czasownik"},
                new WordTypeOption {Type = Types.Adjective, Label = "Przymiotnik"},
                new WordTypeOption {Type = Types.Pronoun, Label = "Zaimek"},
                new WordTypeOption {Type = Types.Preposition, Label = "Przyimek"},
                new WordTypeOption {Type = Types.Conjunction, Label = "Spójnik"},
                new WordTypeOption {Type = Types.Determiner, Label = "Oznacznik"},
                new WordTypeOption {Type = Types.Exclamation, Label = "Okrzyk"},
                new WordTypeOption {Type = Types.Adverb, Label = "Przysłówek"},
            };
            this.wordType.DisplayMember = "Label";
            this.wordType.ValueMember = "Type";
            this.wordType.SelectedItem = this.wordType.Items[0];
        }

        public void AddWord(object sender, EventArgs args)
        {
            var wordTypeOption = (WordTypeOption)wordType.SelectedItem;
            var levelOption = (LevelOption)level.SelectedItem;

            var newWord = new Word
            {
                Polish = this.polishText.Text,
                English = this.englishText.Text,
                Type = wordTypeOption.Label,
                Level = (int)levelOption.Level
            };

            using (var dbContext = new DrInglish())
            {
                var word = dbContext.Words.Add(newWord);
                dbContext.SaveChanges();
                AddElementToList(word);
            }

            this.level.SelectedItem = this.level.Items[0];
            this.wordType.SelectedItem = this.wordType.Items[0];
            this.polishText.Text = "";
            this.englishText.Text = "";
        }

        public void EditWord(object sender, EventArgs args)
        {
            int wordId = int.Parse((string)(sender as Button).Tag);
            var editingWord = this.Words
                .Where(word => word.Id == wordId)
                .DefaultIfEmpty(null)
                .FirstOrDefault();

            if (editingWord != null)
            {
                var editWordWindow = new EditWordWindow(editingWord);
                editWordWindow.Show();
                this.Hide();
                editWordWindow.FormClosed += OnEditWindowClose;
            }
        }

        public void RemoveWord(object sender, EventArgs args)
        {
            using (var dbContext = new DrInglish())
            {
                var wordId = int.Parse((string)(sender as Button).Tag);

                var toRemove = dbContext.Words
                    .Where(word => word.Id == wordId)
                    .DefaultIfEmpty(null)
                    .FirstOrDefault();

                if (toRemove != null)
                {
                    dbContext.Words.Remove(toRemove);
                    dbContext.SaveChanges();
                }

                RenderList();
            }
        }

        private void OnEditWindowClose(object sender, EventArgs args)
        {
            this.Show();

            if ((sender as EditWordWindow).EditedWord == null)
                return;

            var toRemove = this.Words
                .Where(word => word.Id == (sender as EditWordWindow).EditedWord.Id)
                .DefaultIfEmpty(null)
                .FirstOrDefault();

            this.Words.Remove(toRemove);
            this.Words.Add((sender as EditWordWindow).EditedWord);
        }

        private void DictionaryWindow_Load(object sender, EventArgs e)
        {
            RenderList();
        }

        private void RenderList()
        {
            if (this.WordsContainer == null)
            {
                this.WordsContainer = new TableLayoutPanel();
                this.WordsContainer.AutoSize = true;
                this.WordsContainer.ColumnCount = 5;
                this.WordsContainer.RowCount = 0;
                this.WordsContainer.Width = 460;
                this.WordsContainer.BackColor = System.Drawing.Color.White;
                this.WordsContainer.Margin = new Padding(0, 10, 0, 10);

                this.WordsPanel.Controls.Add(this.WordsContainer);
            }
            else
            {
                this.WordsContainer.Controls.Clear();
                this.WordsContainer.RowCount = 0;
            }


            using (var dbContext = new DrInglish())
            {
                var words = dbContext.Words
                .OrderBy(w => w.Level)
                .Select(w => w)
                .ToList<Word>();

                this.Words = new List<Word>();

                foreach (var word in words)
                {
                    AddElementToList(word);
                }
            }
        }

        private void AddElementToList(Word word)
        {
            this.Words.Add(word);
            var currentRow = this.WordsContainer.RowCount + 1;
            this.WordsContainer.RowCount = currentRow;

            var editButton = new Button();
            editButton.Click += EditWord;
            editButton.Text = "Edytuj";
            editButton.Tag = word.Id.ToString();
            editButton.Width = 60;
            editButton.BackColor = System.Drawing.Color.Red;
            editButton.ForeColor = System.Drawing.Color.White;

            var removeButton = new Button();
            removeButton.Click += RemoveWord;
            removeButton.Text = "Usuń";
            removeButton.Tag = word.Id.ToString();
            removeButton.Width = 60;
            removeButton.BackColor = System.Drawing.Color.Red;
            removeButton.ForeColor = System.Drawing.Color.White;

            var plLabel = new Label();
            plLabel.Text = word.Polish;
            plLabel.ForeColor = System.Drawing.Color.DarkRed;
            plLabel.Font = new System.Drawing.Font("Raleway", 13);
            plLabel.Width = 120;

            var enLabel = new Label();
            enLabel.Text = word.English;
            enLabel.ForeColor = System.Drawing.Color.DarkRed;
            enLabel.Font = new System.Drawing.Font("Raleway", 13);
            enLabel.Width = 120;

            this.WordsContainer.Controls.Add(editButton, 1, currentRow);
            this.WordsContainer.Controls.Add(removeButton, 2, currentRow);
            this.WordsContainer.Controls.Add(plLabel, 3, currentRow);
            this.WordsContainer.Controls.Add(enLabel, 4, currentRow);
        }
    }
}
