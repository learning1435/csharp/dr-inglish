﻿namespace dr_inglish
{
    partial class MenuWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuWindow));
            this.treningBtn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.exitBtn = new System.Windows.Forms.Button();
            this.dictrionaryBtn = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // treningBtn
            // 
            this.treningBtn.BackColor = System.Drawing.Color.Transparent;
            this.treningBtn.BackgroundImage = global::dr_inglish.Properties.Resources.Button_noshadow;
            this.treningBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.treningBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.treningBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.treningBtn.Font = new System.Drawing.Font("Raleway", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treningBtn.ForeColor = System.Drawing.Color.White;
            this.treningBtn.Location = new System.Drawing.Point(50, 65);
            this.treningBtn.Margin = new System.Windows.Forms.Padding(0);
            this.treningBtn.Name = "treningBtn";
            this.treningBtn.Size = new System.Drawing.Size(193, 47);
            this.treningBtn.TabIndex = 4;
            this.treningBtn.Text = "Trening";
            this.treningBtn.UseVisualStyleBackColor = false;
            this.treningBtn.Click += new System.EventHandler(this.openTrening);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.exitBtn);
            this.groupBox1.Controls.Add(this.dictrionaryBtn);
            this.groupBox1.Controls.Add(this.treningBtn);
            this.groupBox1.Location = new System.Drawing.Point(318, 152);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(294, 334);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            // 
            // exitBtn
            // 
            this.exitBtn.BackColor = System.Drawing.Color.Transparent;
            this.exitBtn.BackgroundImage = global::dr_inglish.Properties.Resources.Button_noshadow;
            this.exitBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.exitBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exitBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.exitBtn.Font = new System.Drawing.Font("Raleway", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitBtn.ForeColor = System.Drawing.Color.White;
            this.exitBtn.Location = new System.Drawing.Point(50, 246);
            this.exitBtn.Margin = new System.Windows.Forms.Padding(0);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(193, 47);
            this.exitBtn.TabIndex = 8;
            this.exitBtn.Text = "Wyjście";
            this.exitBtn.UseVisualStyleBackColor = false;
            this.exitBtn.Click += new System.EventHandler(this.exitApp);
            // 
            // dictrionaryBtn
            // 
            this.dictrionaryBtn.BackColor = System.Drawing.Color.Transparent;
            this.dictrionaryBtn.BackgroundImage = global::dr_inglish.Properties.Resources.Button_noshadow;
            this.dictrionaryBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.dictrionaryBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dictrionaryBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.dictrionaryBtn.Font = new System.Drawing.Font("Raleway", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dictrionaryBtn.ForeColor = System.Drawing.Color.White;
            this.dictrionaryBtn.Location = new System.Drawing.Point(50, 155);
            this.dictrionaryBtn.Margin = new System.Windows.Forms.Padding(0);
            this.dictrionaryBtn.Name = "dictrionaryBtn";
            this.dictrionaryBtn.Size = new System.Drawing.Size(193, 47);
            this.dictrionaryBtn.TabIndex = 7;
            this.dictrionaryBtn.Text = "Słownik";
            this.dictrionaryBtn.UseVisualStyleBackColor = false;
            this.dictrionaryBtn.Click += new System.EventHandler(this.openDictionary);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::dr_inglish.Properties.Resources.flag_noshadow;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(404, 52);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(111, 54);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Raleway", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Indigo;
            this.label1.Location = new System.Drawing.Point(369, 130);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(25, 10, 25, 10);
            this.label1.Size = new System.Drawing.Size(189, 54);
            this.label1.TabIndex = 7;
            this.label1.Text = "Dr Inglish";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MenuWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::dr_inglish.Properties.Resources.background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(932, 591);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MenuWindow";
            this.Text = "Dr Inglish";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button treningBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Button dictrionaryBtn;
        private System.Windows.Forms.Label label1;
    }
}

