﻿namespace dr_inglish
{
    partial class TrainingWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TrainingWindow));
            this.currentWordIndex = new System.Windows.Forms.Label();
            this.separator = new System.Windows.Forms.Label();
            this.allWordsCount = new System.Windows.Forms.Label();
            this.results = new System.Windows.Forms.GroupBox();
            this.nextWordsBtn = new System.Windows.Forms.Button();
            this.currentWordInput = new System.Windows.Forms.TextBox();
            this.translationContainer = new System.Windows.Forms.GroupBox();
            this.wordToTranslate = new System.Windows.Forms.Label();
            this.results.SuspendLayout();
            this.translationContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // currentWordIndex
            // 
            this.currentWordIndex.AutoSize = true;
            this.currentWordIndex.BackColor = System.Drawing.Color.Transparent;
            this.currentWordIndex.Font = new System.Drawing.Font("Raleway", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentWordIndex.ForeColor = System.Drawing.Color.White;
            this.currentWordIndex.Location = new System.Drawing.Point(28, 38);
            this.currentWordIndex.Name = "currentWordIndex";
            this.currentWordIndex.Size = new System.Drawing.Size(36, 44);
            this.currentWordIndex.TabIndex = 0;
            this.currentWordIndex.Text = "1";
            // 
            // separator
            // 
            this.separator.AutoSize = true;
            this.separator.BackColor = System.Drawing.Color.Transparent;
            this.separator.Font = new System.Drawing.Font("Raleway", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.separator.ForeColor = System.Drawing.Color.White;
            this.separator.Location = new System.Drawing.Point(87, 38);
            this.separator.Name = "separator";
            this.separator.Size = new System.Drawing.Size(38, 44);
            this.separator.TabIndex = 1;
            this.separator.Text = "z";
            // 
            // allWordsCount
            // 
            this.allWordsCount.AutoSize = true;
            this.allWordsCount.BackColor = System.Drawing.Color.Transparent;
            this.allWordsCount.Font = new System.Drawing.Font("Raleway", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.allWordsCount.ForeColor = System.Drawing.Color.White;
            this.allWordsCount.Location = new System.Drawing.Point(143, 38);
            this.allWordsCount.Name = "allWordsCount";
            this.allWordsCount.Size = new System.Drawing.Size(63, 44);
            this.allWordsCount.TabIndex = 2;
            this.allWordsCount.Text = "20";
            // 
            // results
            // 
            this.results.BackColor = System.Drawing.Color.Transparent;
            this.results.Controls.Add(this.currentWordIndex);
            this.results.Controls.Add(this.allWordsCount);
            this.results.Controls.Add(this.separator);
            this.results.Font = new System.Drawing.Font("Raleway", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.results.ForeColor = System.Drawing.Color.White;
            this.results.Location = new System.Drawing.Point(594, 12);
            this.results.Name = "results";
            this.results.Size = new System.Drawing.Size(212, 100);
            this.results.TabIndex = 3;
            this.results.TabStop = false;
            this.results.Text = "Postęp";
            // 
            // nextWordsBtn
            // 
            this.nextWordsBtn.BackColor = System.Drawing.Color.Transparent;
            this.nextWordsBtn.BackgroundImage = global::dr_inglish.Properties.Resources.Button_noshadow;
            this.nextWordsBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.nextWordsBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nextWordsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.nextWordsBtn.Font = new System.Drawing.Font("Raleway", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nextWordsBtn.ForeColor = System.Drawing.Color.White;
            this.nextWordsBtn.Location = new System.Drawing.Point(313, 411);
            this.nextWordsBtn.Margin = new System.Windows.Forms.Padding(0);
            this.nextWordsBtn.Name = "nextWordsBtn";
            this.nextWordsBtn.Size = new System.Drawing.Size(193, 47);
            this.nextWordsBtn.TabIndex = 5;
            this.nextWordsBtn.Text = "Następny";
            this.nextWordsBtn.UseVisualStyleBackColor = false;
            this.nextWordsBtn.Click += new System.EventHandler(this.NextWord);
            // 
            // currentWordInput
            // 
            this.currentWordInput.Font = new System.Drawing.Font("Raleway", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentWordInput.Location = new System.Drawing.Point(251, 318);
            this.currentWordInput.Name = "currentWordInput";
            this.currentWordInput.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.currentWordInput.Size = new System.Drawing.Size(308, 45);
            this.currentWordInput.TabIndex = 6;
            this.currentWordInput.WordWrap = false;
            // 
            // translationContainer
            // 
            this.translationContainer.BackColor = System.Drawing.Color.Transparent;
            this.translationContainer.Controls.Add(this.wordToTranslate);
            this.translationContainer.Font = new System.Drawing.Font("Raleway", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.translationContainer.ForeColor = System.Drawing.Color.White;
            this.translationContainer.Location = new System.Drawing.Point(251, 209);
            this.translationContainer.Name = "translationContainer";
            this.translationContainer.Size = new System.Drawing.Size(308, 89);
            this.translationContainer.TabIndex = 7;
            this.translationContainer.TabStop = false;
            this.translationContainer.Text = "Przetłumacz";
            // 
            // wordToTranslate
            // 
            this.wordToTranslate.AutoSize = true;
            this.wordToTranslate.Location = new System.Drawing.Point(55, 43);
            this.wordToTranslate.MinimumSize = new System.Drawing.Size(200, 0);
            this.wordToTranslate.Name = "wordToTranslate";
            this.wordToTranslate.Size = new System.Drawing.Size(200, 19);
            this.wordToTranslate.TabIndex = 0;
            this.wordToTranslate.Text = "-----------";
            this.wordToTranslate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TrainingWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::dr_inglish.Properties.Resources.background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(818, 535);
            this.Controls.Add(this.translationContainer);
            this.Controls.Add(this.currentWordInput);
            this.Controls.Add(this.nextWordsBtn);
            this.Controls.Add(this.results);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TrainingWindow";
            this.Text = "Trening";
            this.Shown += new System.EventHandler(this.OnWindowLoad);
            this.results.ResumeLayout(false);
            this.results.PerformLayout();
            this.translationContainer.ResumeLayout(false);
            this.translationContainer.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label currentWordIndex;
        private System.Windows.Forms.Label separator;
        private System.Windows.Forms.Label allWordsCount;
        private System.Windows.Forms.GroupBox results;
        private System.Windows.Forms.Button nextWordsBtn;
        private System.Windows.Forms.TextBox currentWordInput;
        private System.Windows.Forms.GroupBox translationContainer;
        private System.Windows.Forms.Label wordToTranslate;
    }
}