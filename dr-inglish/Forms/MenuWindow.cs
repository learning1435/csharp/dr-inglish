﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dr_inglish
{
    public partial class MenuWindow : Form
    {
        TrainingWindow trainigWindow;
        DictionaryWindow dictionaryWindow;

        public MenuWindow()
        {
            InitializeComponent();
        }

        private void exitApp(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void openTrening(object sender, EventArgs e)
        {
            this.trainigWindow = new TrainingWindow();
            this.Hide();
            this.trainigWindow.Show();
            this.trainigWindow.FormClosing += onChildWindowClose;
            this.trainigWindow = null;
        }

        private void openDictionary(object sender, EventArgs e)
        {
            this.dictionaryWindow = new DictionaryWindow();
            this.Hide();
            this.dictionaryWindow.Show();
            this.dictionaryWindow.FormClosing += onChildWindowClose;
        }

        private void onChildWindowClose(Object sender, FormClosingEventArgs e)
        {
            this.Show();
        }
    }
}
