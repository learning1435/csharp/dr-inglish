﻿using dr_inglish.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace dr_inglish.Forms
{
    public partial class EditWordWindow : Form
    {
        public Word EditedWord { get; set; }

        private Word PassedWord { get; set; }

        public EditWordWindow(Word word)
        {
            InitializeComponent();
            this.PassedWord = word;
            LoadWordData();
        }

        public void LoadWordData()
        {
            this.polishText.Text = this.PassedWord.Polish;
            this.englishText.Text = this.PassedWord.English;

            var levelOptions = new List<LevelOption>()
            {
                new LevelOption {Level = Levels.Beginner, Label = "Początkujący"},
                new LevelOption {Level = Levels.Medium, Label = "Zaawansowany" },
                new LevelOption {Level = Levels.Expert, Label = "Ekspert"}
            };

            this.level.DataSource = levelOptions;

            this.level.DisplayMember = "Label";
            this.level.ValueMember = "Level";

            var selectedLevel = levelOptions
                .Where(o => (int)o.Level == this.PassedWord.Level)
                .DefaultIfEmpty(null)
                .FirstOrDefault();

            if (selectedLevel == null)
                this.level.SelectedItem = levelOptions.First();
            else
                this.level.SelectedItem = selectedLevel;


            var typeOptions = new List<WordTypeOption>()
            {
                new WordTypeOption {Type = Types.Noun, Label = "Rzeczownik"},
                new WordTypeOption {Type = Types.Verb, Label = "Czasownik"},
                new WordTypeOption {Type = Types.Adjective, Label = "Przymiotnik"},
                new WordTypeOption {Type = Types.Pronoun, Label = "Zaimek"},
                new WordTypeOption {Type = Types.Preposition, Label = "Przyimek"},
                new WordTypeOption {Type = Types.Conjunction, Label = "Spójnik"},
                new WordTypeOption {Type = Types.Determiner, Label = "Oznacznik"},
                new WordTypeOption {Type = Types.Exclamation, Label = "Okrzyk"},
                new WordTypeOption {Type = Types.Adverb, Label = "Przysłówek"},
            };

            this.wordType.DataSource = typeOptions;

            var selectedType = typeOptions
                .Where(o => o.Label == this.PassedWord.Type)
                .DefaultIfEmpty(null)
                .FirstOrDefault();

            this.wordType.DisplayMember = "Label";
            this.wordType.ValueMember = "Type";

            if (selectedType == null)
                this.wordType.SelectedItem = typeOptions.First();
            else
                this.wordType.SelectedItem = selectedType;

        }

        public void SaveChanges(object sender, EventArgs args)
        {
            this.EditedWord = new Word()
            {
                Id = this.PassedWord.Id,
                Polish = this.polishText.Text,
                English = this.englishText.Text,
                Level = (int)(this.level.SelectedItem as LevelOption).Level,
                Type = (this.wordType.SelectedItem as WordTypeOption).Label
            };

            this.Close();
        }
    }
}
