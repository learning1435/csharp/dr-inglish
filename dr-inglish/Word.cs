namespace dr_inglish
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Word
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(250)]
        public string Polish { get; set; }

        [Required]
        [StringLength(250)]
        public string English { get; set; }

        [Required]
        [StringLength(100)]
        public string Type { get; set; }

        public int Level { get; set; }
    }
}
