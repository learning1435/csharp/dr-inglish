﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dr_inglish.Helpers
{
    class Helpers
    {
    }

    public class WordTypeOption
    {
        public Types Type { get; set; }
        public string Label { get; set; }
    }

    public class LevelOption
    {
        public Levels Level { get; set; }
        public string Label { get; set; }
    }

    public enum Levels
    {
        Beginner,
        Medium,
        Expert
    };

    public enum Types
    {
        Noun,
        Verb,
        Adjective,
        Adverb,
        Pronoun,
        Preposition,
        Conjunction,
        Determiner,
        Exclamation
    }
}
